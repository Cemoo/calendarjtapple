//
//  CustomCell.swift
//  TestJTCalendar
//
//  Created by Cemal Bayrı on 30/08/2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {
    
    @IBOutlet weak var lblCalender: UILabel!
    override func awakeFromNib() {
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 2
    }
}
