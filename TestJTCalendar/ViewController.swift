//
//  ViewController.swift
//  TestJTCalendar
//
//  Created by Cemal Bayrı on 30/08/2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit
import JTAppleCalendar

class ViewController: UIViewController {

    @IBOutlet weak var calenderView: JTAppleCalendarView!
    
    let formatter = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        calenderView.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        calenderView.minimumLineSpacing = 0
        calenderView.minimumInteritemSpacing = 0
        
        formatter.dateFormat = "yyyy MM dd"
        let date = formatter.date(from: "2017 01 03")!
        calenderView.selectDates([date])
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = TimeZone(abbreviation: "BST")
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2017 01 01 ")
        let endDate = formatter.date(from: "2017 12 31")
        
        let params = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        
        return params
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.lblCalender.text = cellState.text
        if cellState.dateBelongsTo == .thisMonth {
            cell.lblCalender.textColor = UIColor.white
        }
        else {
            cell.lblCalender.textColor = UIColor.gray

        }
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        if let validCell = cell as? CustomCell {
            validCell.lblCalender.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
            print(cellState.date)
            
            
        } else {
            return
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        formatter.dateFormat = "yyyy"
        print(formatter.string(from: date))
        
        formatter.dateFormat = "MMMM"
        print(formatter.string(from: date))
    }
}







