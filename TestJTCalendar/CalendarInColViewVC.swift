//
//  CalendarInColViewVC.swift
//  TestJTCalendar
//
//  Created by Cemal Bayrı on 08/09/2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit

class CalendarInColViewVC: UIViewController {

    @IBOutlet weak var colCalendar: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        colCalendar.delegate = self

        let nib = UINib(nibName: "CalendarView", bundle: nil)
        colCalendar.register(nib, forCellWithReuseIdentifier: "cell2")
        
        colCalendar.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellNormal")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}

extension CalendarInColViewVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = colCalendar.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! CalendarView
            cell.calendar_View.reloadData()
            return cell
        }
        else {
            let cell = colCalendar.dequeueReusableCell(withReuseIdentifier: "cellNormal", for: indexPath)
            cell.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 300)
    }
    
    
}
