//
//  CalendarColViewCell.swift
//  TestJTCalendar
//
//  Created by Cemal Bayrı on 08/09/2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarColViewCell: JTAppleCell {
    
    @IBOutlet weak var bckgrVşw: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
    }
    
    
}
