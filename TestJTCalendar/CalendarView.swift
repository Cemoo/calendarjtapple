//
//  CalendarView.swift
//  TestJTCalendar
//
//  Created by Cemal Bayrı on 08/09/2017.
//  Copyright © 2017 Cemal Bayrı. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarView: UICollectionViewCell {
    
    @IBOutlet weak var calendar_View: JTAppleCalendarView!
    
    let formatter = DateFormatter()
    override func awakeFromNib() {
        calendar_View.minimumLineSpacing = 0
        calendar_View.minimumInteritemSpacing = 0
        calendar_View.isPagingEnabled = true
        calendar_View.allowsMultipleSelection = true
    
        let nib = UINib(nibName
            : "CalendarColViewCell", bundle: nil)
        calendar_View.register(nib, forCellWithReuseIdentifier: "CustomCell")
        
        formatter.dateFormat = "yyyy MM dd"
        calendar_View.selectDates([formatter.date(from: "2017 09 09")!])

    }
}
extension CalendarView: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2017 01 01 ")
        let endDate = formatter.date(from: "2017 12 31")
        
        let params = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        
        
        return params
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CalendarColViewCell
        
       
        cell.lblDate.text = cellState.text
        if cellState.dateBelongsTo == .thisMonth {
            cell.lblDate.textColor = UIColor.darkGray
            
        }
        else {
            cell.lblDate.textColor = UIColor.lightGray
        }
        
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        if let validCell = cell as? CalendarColViewCell {
            validCell.bckgrVşw.backgroundColor = UIColor.yellow
            print(cellState.date)
            
        } else {
            return
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        if let validCell = cell as? CalendarColViewCell {
            calendar_View.deselect(dates: [cellState.date])
            validCell.bckgrVşw.backgroundColor = UIColor.white
            validCell.lblDate.textColor = UIColor.darkGray
            
        } else {
            return
        }
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        formatter.dateFormat = "yyyy"
        print(formatter.string(from: date))
        
        formatter.dateFormat = "MMMM"
        print(formatter.string(from: date))
    }
    
    
}
